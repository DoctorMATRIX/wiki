[Français](/Translated/French/Rules/AdvancementRules.md) | [Español](/Translated/Spanish/Rules/AdvancementRules.md) | [Romanian](/Translated/Romanian/Rules/AdvancementRules.md) | [English](/Rules/AdvancementRules.md)

---
# Osiągnięcia - Zasady
Tutaj możesz znaleźć więcej informacji na temat zasady, która zabrania Ci ujawniać lokalizacji osiągnięć na serwerze.

Ogólnie rzecz biorąc, nie możesz nikomu powiedzieć, jak ukończyć jakieś osiągnięcie, dla przykładu: "O tak, dziannik Nathana jest (jego lokalizacja)", lub "Po prostu wejdź do (tego budynku)".

## Współpracowanie
Jeśli ty i twoim znajomi chcecie razem zebrać osiągnięcia, to macie na to pozwolenie, **JEŚLI** żaden z was nie ma już tych osiągnięć (nie możesz nikogo poprowadzić do uzyskania osiągnięć).

Dodatkowow nie możesz dzielic pracy między siebie i znajomych. Musisz szukać razem z nimi w jednej grupie. Nie możesz powiedzieć swoim znajomym, aby poszli szukać w miejscu A, podczas gdy ty szukasz w miejscu B. W skrócie: **musicie się trzymać razem**, ponadto jeśli, któryś z twoich znajomych znajdzie coś podczas bycia solo, nie może Ci niczego powiedzieć.

## Wyjątki

### Rozgrywka
**Zasada kciuka: możesz przedstawić rozgrywkę innym graczom.**

Oznacza to, że możesz powiedzieć innym, jak mają wykonywać czynności związane z rozgrywką. To działanie może przypadkowo doporowadzić do odkrycia osiągnięcia, więc są one wyłączone z powyższych zasad. 

Listę możesz znaleźć [tutaj](#Lista)
### Osiągnięcia kooperacyjne
Wyjątek stanowią też osiągnięcia, które wymagają więcej niż jednego gracza do ukończenia.

Możesz oczywiście poprosić innych graczy, aby dołączyli do Ciebie podczas zdobywania tych osiągnięć. Zachęcamy jednak, abyś nie spoilerował szczegółów, aby pozostały one interesujące dla innych graczy. 

### Lista
Oto pełna lista osiągnięć, które są związane z rozgrywką, które możesz wyjaśnić innym graczom.

**Ta lista może być nie aktualna, jeśli masz wątpliwość zapytaj Administratora.**
- Kadic
    * Masz zamiar to dokończyć?
    * Sos... To potrzebuje więcej sosu
    * Jagnięcina i ziemniaki na deser
    * Najwspanialszy Chłopiec
    * Miłośnik Windows'a
    * R.I.P.
    * Ale dlaczego ona jest szara?
- Factory
    * Déjà Vu
- Lyoko
    * Wszystkie (z kategorii Lyoko)

**Zwróć uwagę, że żadne z osiągnięć z Sektora 5-go nie jest wykluczone**


## Konsekwencje
Jeśli okaże się, że pomagasz innym przy osiągnięciach, to może stać się jedna z następujących rzeczy; możes zostać:
* Upomniany
* Wykluczony z możliwości zdobycia Veterana
* Zbanowany tymczasowo
* Zbanowany na zawsze

Zazwyczaj kary będą nakładane w tej kolejności, no chyba że celowo złamiesz zasady.