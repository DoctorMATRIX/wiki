[Français](/Translated/French/Rules/Rules.md) | [Español](/Translated/Spanish/Rules/Rules.md) | [Romanian](/Translated/Romanian/Rules/Rules.md) | [English](/Rules/Rules.md)

---
# Zasady
Zasady zawarte w tym dokumencie dotyczą Discorda i serwera Minecraft. Są wskazówką, a ostateczna decyzja zawsze należy do Administracji i Moderacji. Są również wymienione w przypadkowej kolejności, z wyjątkiem pierwszej zasady

## Pierwsza Zasada
Pierwszą i zarazem najważniejszą zasadą jest: **używaj logicznego myślenia**. 

Nie przeszkadzaj innym i bądź przyzwoitym człowiekiem. Nie każ nam dodawać zasad ze względu na Ciebie tylko dlatego, że przeceniliśmy ludzką przyzwoitość.

## Zasady ogólne

- Decyzje Moderacji są ostateczne. Jeśli chcesz złożyć skargę, skontaktuj się z administratorem lub Jackiem, jeśli chcesz złożyć skargę na administratora.

- **Mówienie komukolwiek, gdzie i jak zdobyć osiągnięcie jest niedozwolone.** Możesz jednak wyjaśnić mechaniki gry. Pełną listę tego, co jest, a co nie jest dozwolone, można znaleźć [tutaj](/Translated/Polish/Rules/AdvancementRules.md).

- Spam jest zabroniony. W zależności od kanału ta reguła jest egzekwowana w różny sposób. Zobacz [Używanie Kanałów](/Translated/Polish/Rules/ChannelUsage.md) po więcej szczegółów.

- Możesz kłócić/dyskutować o wszystkim, ale my wyznaczamy granicę jeśli chodzi o wyzwiska. Jeśli chcesz się z kimś obrażać, przejdź na wiadomości prywatne.

- Rasizm i wszelkiego rodzaju dyskryminacja nie są czymś, co tolerujemy.

- Nie mamy zakazu na przeklinanie, ale kieruj się zdrowym rozsądkiem i nie przesadzaj.

- Możesz **tylko** reklamować projekty związane z Kod Lyoko w granicach rozsądku. To zależy od uznania moderatora. Jeśli chcesz zareklamować jakiś inny projekt, poproś najpierw administratora.

- **Mamy ograniczenia dotyczące nazw użytkowników i pseudonimów, o których możesz przeczytać [tutaj](/Translated/Polish/Rules/NamingRules.md)**

- Podstawowym językiem jest Angielski, ale możesz mówić w innych językach na serwerze lub na [kanałach językowych](/Translated/Polish/Rules/ChannelUsage.md#Kanały-językowe)

- Konta alternatywne są niedozwolone.

- Treści NSFW, takie jak nagość lub pornografia, są niedozwolone. Żarty i memy są do pewnego stopnia dozwolone na odpowiednich [kanałach](/Translated/Polish/Rules/ChannelUsage.md)

- Unikanie kary sprawi, że ... znowu zostaniesz zbanowany. Na stałe. Wydaje się to całkiem logiczne, ale musimy napisać tą zasadę.

- **Bycie świnią dla innych ludzi w wiadomościach prywatnych lub na innych serwerach może również spowodować, że zostaniesz tu zbanowany**
## Specjalne zasady dla Discorda

- Nadmierne wspominanie/@'owanie kogokolwiek bez powodu spowoduje wyciszenie lub coś gorszego.

- Trollowanie/denerwowanie ludzi za pomocą botów jest niedozwolone. Jeśli ktoś zacznie narzekać, zawsze będziesz w błędzie.

## Specjalne zasady dla Minecrafta
- Trolling przy użyciu mechaniki gry jest niedozwolony. Przykłady obejmują między innymi: 
    * Zmiana miejsca docelowego skanera bez pytania
    * Spamowanie Powrotem do Przeszłości
    * Wielokrotne atakowanie wbrew czyjejś woli bez bycia zxanafikowanym