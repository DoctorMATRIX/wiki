[Français](/Translated/French/Rules/ChannelUsage.md) | [Español](/Translated/Spanish/Rules/ChannelUsage.md) | [Romanian](/Translated/Romanian/Rules/ChannelUsage.md) | [English](/Rules/ChannelUsage.md)

---
# Używanie kanałów
Tutaj możesz się dowiedzieć jaką rolę pełnią poszczególne kanały na naszym [Serwerze Discord](https://discord.mrcl.cc).

**Jeśli chcesz pisać w innym języku niż angielski, to kliknij [tutaj](#Kanały-językowe), aby znaleźć odpowiednie do tego kanały.**

# Kategoria główna
### \#general
Kanał general jest przeznaczony do poważniejszych rozmów na poważne tematy. Wysyłanie zbędnych rzeczy (Shitposting) jest zabronione, a memy są dozwolone tylko, gdy dotyczą poruszanego tematu. Po prostu nie publikuj żadnych przypadkowych memów, gdy inni prowadzą poważną rozmowę.

### \#sewers
Kanał sewers jest przeznaczony do publikowania zbędnych rzeczy (Shitposting), memów oraz randomowych linków. To nie oznacza jednak, że możesz tam spamować, ale możesz być na tym kanale trochę bardziej wyluzowany.

### \#bot-spam
Kanał bot-spam jest przeznaczony do zabawy z botami. Ten kanał nie jest często kontrolowany, więc pamiętaj aby wezwać moderatora, gdy ktoś będzie się źle tam zachowywać. Ten kanał z natury ma dużo spamu, więc zasada zakazu spamu jest traktowana na nim w lużny sposób.

### \#voice-chat
Ten kanał jest przeznaczony, dla tych, którzy chcą się integrować za pomocą tekstu z osobami prowadzącymi czat głosowy. Nie uzywaj tutaj botów muzycznych, zachowaj to dla #bot-spam.

### \#suggestions
Ten kanał służy do publikowania waszych sugestii dotyczących serwera Discord lub Minecraft. Pamiętaj, że Twoja sugestia mogła się już pojawić wcześniej, więc niektóre z nich możemy zignorować. To idealne miejsce na burzę mózgów.

# Kanały językowe
Jesteśmy otwarci na osoby mówiące w innych językach. Obecnie mamy dostępne następujące kanały:
- \#fr-talk
- \#es-talk
- \#fi-talk

Te kanały służą do mówienie w odpowiednich językach, lub proszenia o pomoc przy nich (np. "Jak powiedzieć "bagietka" po francusku?")

Jeśli chcesz kanał dla swojego języka, skontaktuj się z administratorem i może to rozważymy.

# Kategoria różnorodna
Nie wszystkie kanały będą tutaj przedstawione, tylko te z określonymi zasadami.

### \#head-area-exposing-channel
Ten kanał służy do ujawniania swojej twarzy. Nie publikuj swoich nagich zjęć (ani cudzych) i nie publikuj fałszywych zdjęć "swojej" twarzy, to nie jest zabawne. **Nie ujwaniaj twarzy kogoś innego**.

### \#not-safe-for-kids
**Pornografia i nagość nie jest tutaj dozwolona.**

NSFK to kanał, na którym możesz publikowac swoje dark memes lub treści, które generalnie nie są odpowiednie dla powiedzmy, 14-latków. Możesz tutaj też porozmawiać na bardziej delikatne tematy, takie jak seksualność, polityka lub orientacja seksualna. (Zwróć uwagę, że te kanały nie są jako tako zakazane na pozostałych kanałach, ten jest tylko kanałem dedykowanym).

### \#mrclmemes
Ten kanał jest **tylko dla memów**. 
**Nie** komentuj zamieszczanych tutaj memów. Ma to na celu zarchiwizowanie wszystkich memów związanych z MRCLem w jednym miejscu.

### \#free-games
Na tym kanale możesz publikowac gry, które są **dostępne za darmo** przez ograniczony czas. Mamy też bota, który czasami umieszcza tutaj linki. (Nie odpowiadamy za treści wysyłane przez bota)

### \#screenshots
Na tym kanale możesz wysyłać swoje piękne zrzuty ekranu z MRCLa. **Komentowanie jest zabronione**. Ten kanał nie służy także do zgłaszania błędów, odpowiedni kanał do tego to #bug-report.

Jeśli jakiś zrzut ekranu Ci się podoba, możesz zareagować emotką :pushpin:, a my go przypniemy jeśli wystarczająca ilość osób doda reakcję.

Możemy wykorzystać te zrzuty ekranu na naszej stronie internetowej lub innych mediach.