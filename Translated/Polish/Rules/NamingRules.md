[Français](/Translated/French/Rules/NamingRules.md) | [Español](/Translated/Spanish/Rules/NamingRules.md) | [Romanian](/Translated/Romanian/Rules/NamingRules.md) | [English](/Rules/NamingRules.md)

---
# Nazwy - zasady
Mamy bardzo specyficzny zestaw zasad dotyczących naz użytkowników zarówna na Discordzie, jak i w Minecrafcie.

## Nazwy, które są zabronione
Nie możesz mieć nazwy, która:
* Przypomina imię postaci w serialu
* Nie zawiera identyfikowalnego zestawu normalnych znaków
* Nie możne udawać imienia
* Nie możne być łatwo oznaczona na Discordzie

## Co robić, jeśli masz niedozwoloną nazwę
To zależy. Tutaj jest kilka scenariuszy:

### Niedozwolona nazwa to twoja nazwa w grze
Jeśli masz legalną kopię gry Minecraft i nie chcesz zmieniać swojej nazwy użytkownika za pośrednictwem strony mojang.com, poproś moderatora o zmianę pseudonimu na inny.

**Jeśli nie posiadasz Minecrafta, zmień swoja nazwę w swoim launcherze lub *zostaniesz* zbanowany**

### Niedozwolona nazwa to twoja nazwa Discord
Jeśli nie chcesz zmieniać swojej nazwy użytkownika na Discordzie, użyj funkcji pseudonimu (albo zrobimy to za Ciebie, a nie spodoba ci się to, co wybierzemy)

## Przykłady niedozwolonych nazw
### 11ε ƉơƈŧēůƦ
Niedozwolona z powodu braku normalnych znaków
### Aelita_Shaeffer
Niedozwolona, bo jest to imię postaci z KLa
### Jeremie
To co wyżej. Jeśli twoje imię to Jeremie to skontaktuj sie z Administracją
### gksqzeoKKSdqslAZRf
Niedozwolone, ponieważ nie można jej używać jako imienia. Nie możesz oczekiwać, że powiemy „hej, gksqzeoKKSdqslAZRf, jak się masz?”